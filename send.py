#!/usr/bin/env python3
from __future__ import annotations

import smtplib
import sys
import typing
import subprocess as sub
from os import path, getenv
import unicodedata
from email.message import EmailMessage
from email.parser import Parser as EmailParser
from email.policy import default as default_email_policy

import jinja2
import premailer
from dotenv import load_dotenv

load_dotenv()


def str_to_bool(s: str) -> bool:
    return s.lower() in ['true', '1', 't', 'y', 'yes', 'yeah', 'yup', 'certainly', 'uh-huh']


class TemplateLoader(typing.Protocol):
    def build(self, name: str, data: typing.Mapping[str, str]) -> str:
        ...


class JinjaTemplateLoader:
    def __init__(self, template_dir: str = "src"):
        self.template_dir = template_dir
        self.template_loader = jinja2.FileSystemLoader(self.template_dir)
        self.template_env = jinja2.Environment(loader=self.template_loader)

    def build(self, name: str, data: typing.Mapping[str, str]) -> str:
        template_name = path.join("emails", name)
        return self.template_env.get_template(name=template_name).render(data)


class MessageSender(typing.Protocol):
    def send(self, message: str):
        ...


class MailHogSender:
    def __init__(self) -> None:
        self.smtp_server = smtplib.SMTP(host="localhost", port=1025)

    def send(self, message: str) -> None:
        message = unicodedata.normalize("NFKD", message)
        msg = EmailParser(policy=default_email_policy).parsestr(message)
        self.smtp_server.send_message(msg)


class SMTPSender:
    def __init__(self) -> None:
        self.smtp_info: typing.Mapping[str, typing.Any] = {
            "host": getenv("SMTP_SERVER_HOST", ""),
            "port": int(getenv("SMTP_SERVER_PORT", "465")),
            "ssl": str_to_bool(getenv("SMTP_SERVER_USE_SSL", "1")),
            "user": getenv("SMTP_SERVER_USER", ''),
            "password": getenv("SMTP_SERVER_PASSWORD", '')
        }

        smtp_cls = smtplib.SMTP_SSL if self.smtp_info["ssl"] else smtplib.SMTP
        self.smtp_server = smtp_cls(
            host=self.smtp_info['host'], port=self.smtp_info['port'])
        self.smtp_server.login(
            self.smtp_info['user'], self.smtp_info['password'])

    def send(self, message: str) -> None:
        message = unicodedata.normalize("NFKD", message)
        msg = EmailParser(policy=default_email_policy).parsestr(message)
        self.smtp_server.send_message(msg)


class TemplateProcessor(typing.Protocol):
    def process(self, template: str) -> str:
        ...


class InlineStyleProcessor:
    def build_css(self) -> str:
        cmd = ["tailwindcss", "-m", "-i", "./src/index.css"]
        return sub.check_output(cmd).decode("utf-8")

    def merge_inline_css(self, template: str, css_text: str) -> str:
        return premailer.transform(
            template, remove_classes=True, css_text=css_text, allow_network=False, keep_style_tags=False, disable_leftover_css=True)

    def process(self, template: str) -> str:
        css_text = self.build_css()
        return self.merge_inline_css(template, css_text)


class MailSigner:
    def sign(self, template: str, sender: str, receivers: typing.List[str], subject: str = "") -> str:
        msg = EmailMessage()
        msg.add_alternative(template, subtype="html")

        if len(subject) > 0:
            msg["Subject"] = subject

        msg['From'] = sender
        msg['To'] = ", ".join(receivers)

        return msg.as_string()


def main(_: typing.Sequence[str]) -> int:
    mail_name = 'hello.jinja'
    mail_sender = getenv('TEST_EMAIL_SENDER', 'test@emeru.com')
    mail_receivers = getenv('TEST_EMAIL_RECEIVERS',
                            'test@emeru.com').split(",")

    loader: TemplateLoader = JinjaTemplateLoader()
    template = loader.build(mail_name, {"name": "Jim"})

    style_processor: TemplateProcessor = InlineStyleProcessor()
    template = style_processor.process(template=template)

    signer: MailSigner = MailSigner()
    mail = signer.sign(template=template, sender=mail_sender,
                       receivers=mail_receivers, subject=mail_name)

    # sender: MessageSender = MailHogSender()
    # sender.send(mail)

    sender: MessageSender = SMTPSender()
    sender.send(mail)

    return 0


if __name__ == "__main__":
    raise SystemExit(main(sys.argv))
