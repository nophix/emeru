/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/components/**/*.jinja",
    "./src/emails/**/*.jinja",
    "./src/layouts/**/*.jinja",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
};
